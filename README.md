PREREQUISITES:
1. Java Virtual Machine (JVM) + SDK.

COMPILATION INSTRUCTIONS:
1. Open Bash.
2. Navigate to the directory containing the "src" directory and the "sources.txt" file.
3. Build the sources by writing:
    javac @sources.txt -d output
4. Done.

EXECUTION INSTRUCTIONS:
1. Open Bash.
2. Navigate to the "output" directory.
3. Execute the program by writing:
    cat << EOF | java com.suprnation.assignment.Launcher
4. Input the triangle tree using the format described in the assignement sheet.
