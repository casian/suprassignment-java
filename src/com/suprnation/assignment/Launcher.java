package com.suprnation.assignment;

import com.suprnation.assignment.datastructures.*;
import java.util.Scanner;

/**
 * The Laucher class hosts the Main method of this application.
 */
public class Launcher {

    /**
     * The main method uses the Scanner class to input the text based
     * triangle tree from the console and uses it to create a TriangleTree
     * object. Once created the minimal path is computed and displayed.
     * @param args
     */
    public static void main(String [] args) {
        //Creating the scanner object
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        //Creating an empty triangle tree
        TriangleTree tree = new TriangleTree();
        boolean treeIsValid = true;

        //Keep on reading from STDIN until EOF or until
        //an invalid level is entered.
        while (scanner.hasNext() && treeIsValid) {
            String line = scanner.next();
            if(!line.equalsIgnoreCase("eof")){
                treeIsValid = tree.addLevel(line);
            }else{
                break;
            }
        }

        //If the tree is valid we compute the minimal path and
        //display the result, else an error is displayed.
        if (treeIsValid) {
            TriangleMinPath path = tree.computeMinimalPath();
            System.out.println(path.toString());
        } else {
            System.out.println("The input triangle tree is invalid.");
        }
    }

}
