package com.suprnation.assignment.datastructures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class defines a list of nodes denoting the minimal path from root to leaf, along
 * with its weight (ie the minimal weight).
 */
public class TriangleMinPath {

    private List<TriangleNode> path = new ArrayList<>();
    private int minWeight;

    public TriangleMinPath(List<TriangleNode> path, int minWeight) {
        this.path = path;
        this.minWeight = minWeight;
    }

    // Getters and Setters
    public List<TriangleNode> getPath() {
        return path;
    }

    public void setPath(List<TriangleNode> path) {
        this.path = path;
    }

    public int getMinWeight() {
        return minWeight;
    }

    public void setMinWeight(int minWeight) {
        this.minWeight = minWeight;
    }


    /**
     *  A custom toString method that analyses the minimal path object and
     *  returns a string version of it.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("The Minimal Path is: ");
        for (Iterator<TriangleNode> i = path.iterator(); i.hasNext(); ) {
            sb.append(i.next().getNumber());
            if (i.hasNext()) {
                sb.append(" + ");
            }
        }
        sb.append(" = ");
        sb.append(minWeight);
        return sb.toString();
    }
}
