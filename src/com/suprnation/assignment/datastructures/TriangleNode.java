package com.suprnation.assignment.datastructures;

/**
 * This class defines a triangle tree node. A node defines a number and its children
 * (one on the left and one on the right) unless it is a leaf. It also defines some
 * placeholder variables that are used during the computation of the minimal path. This
 * includes the weight and the leastWeightedChild (which can be either the left child or
 * the right one).
 */
public class TriangleNode {

    //Private Class Variables
    private int weight;
    private boolean isRoot;
    private boolean isLeaf;
    private int number;

    private TriangleNode childLeft = null;
    private TriangleNode childRight = null;
    private TriangleNode leastWeightedChild = null;

    //Constructors
    public TriangleNode(int number) {
        this(number,false);
    }

    //Constructors
    public TriangleNode(int number, boolean isRoot) {
        this.number = number;
        this.isRoot = isRoot;
        this.isLeaf = true;
    }

    //Getters
    public int getNumber() {
        return number;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public TriangleNode getChildLeft() {
        return childLeft;
    }

    public TriangleNode getChildRight() {
        return childRight;

    }

    // Setters

    public void setRoot(boolean root) {
        isRoot = root;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    public void setChildRight(TriangleNode childRight) {
        if(childRight != null){
            isLeaf=false;
        }
        this.childRight = childRight;
    }

    public void setChildLeft(TriangleNode childLeft) {
        if(childLeft != null){
            isLeaf=false;
        }
        this.childLeft = childLeft;
    }

    public TriangleNode getLeastWeightedChild() {
        return leastWeightedChild;
    }

    public void setLeastWeightedChild(TriangleNode leastWeightedChild) {
        this.leastWeightedChild = leastWeightedChild;
    }


    /**
     * This method computes the weight of this node by adding its number to the weight of its least weighted child.
     */
    public void computeWeights(){
        if(this.isLeaf()) {
            //initializing the weights of the leaves to their own number.
            this.setWeight(this.getNumber());
        }else{
            TriangleNode leftChild = this.getChildLeft();
            TriangleNode rightChild = this.getChildRight();
            TriangleNode chosenChild = leftChild.getWeight() < rightChild.getWeight() ? leftChild : rightChild;
            this.setWeight(this.getNumber()+chosenChild.getWeight());
            this.setLeastWeightedChild(chosenChild);
        }
    }

}
