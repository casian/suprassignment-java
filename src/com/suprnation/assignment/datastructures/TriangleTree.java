package com.suprnation.assignment.datastructures;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * This class defines a TriangleTree consisting of different levels.
 * Starting from the root node, each subsequent level has one more node than its
 * parent level.
 */
public class TriangleTree {

    private List<List<TriangleNode>> levels = new ArrayList<>();

    /**
     * Returns the depth of the current tree.
     */
    public int getDepth() {
        return levels.size();
    }

    /**
     * Returns the list of TriangleNodes for the level stipulated by the level's index
     * @param levelIndex
     * @return The list of TriangleNodes for that level
     */
    public List<TriangleNode> getLevel(int levelIndex){
        return levels.get(levelIndex);
    }


    /**
     * Adds a new level to the triangle tree from the array of numbers provided
     * iff the the length of the array is equal to the current depth+1.
     * @param numbers: an array containing the numbers in the triangle level
     * @return True when the level is valid and has been added successfully, false otherwise.
     */
    public boolean addLevel(int[] numbers){
        if(numbers.length != getDepth()+1){
            return false;
        }else{
            List<TriangleNode> currentLevel = new ArrayList<>();
            levels.add(currentLevel);

            for (int i=0; i<numbers.length; i++) {
                TriangleNode node = new TriangleNode(numbers[i]); //creating a new node for each number in the level
                currentLevel.add(node);
                int depth = getDepth();
                if (depth == 1) { //If the node is the root (ie depth == 1), we must set the root flag.
                    node.setRoot(true);
                }else{ //If the node is not the root (ie depth > 1), we must link it to the previous level.
                    List<TriangleNode> parentLevel = getLevel(depth-2);
                    //add parents + children accordingly

                    if (i >= 1) {
                        TriangleNode parentLeft = parentLevel.get(i - 1);
                        parentLeft.setChildRight(node); //if the parent is on the left, then the child is on the right
                    }
                    if (i < numbers.length - 1) {
                        TriangleNode parentRight = parentLevel.get(i);
                        parentRight.setChildLeft(node); //if the parent is on the right, then the child is on the left
                    }
                }
            }
            return true;
        }
    }

    /**
     * Adds a new level to the triangle tree by parsing a string. The string is parsed correctly only when
     * every number is separated by a whitespace.
     * @param text: a string containing space separated numbers.
     * @return True when the level is valid and has been added successfully, false otherwise.
     */
    public boolean addLevel(String text) {
        String[] levelstr = text.split("\\s+");
        int[] levelnum = new int[levelstr.length];
        try {
            for (int i = 0; i < levelnum.length; i++) {
                levelnum[i] = Integer.parseInt(levelstr[i]);
            }
            return addLevel(levelnum);
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Computes the Minimal path for this tree by starting from the leaf nodes and moves up
     * towards the root. While analysing each tree level of nodes, it computes the weights of
     * each node using node.computeWeights(). This causes each node to compute its weight by
     * adding their own number to the weight of their least weighted child. The result is
     * returned when the weight of the root node is computed.
     * @return The Triangle tree's minimal path.
     */
    public TriangleMinPath computeMinimalPath() {
        ListIterator<List<TriangleNode>> listIter = levels.listIterator(levels.size());
        TriangleNode rootNode = null;
        //Start from bottom most level.
        while (listIter.hasPrevious()) {
            List<TriangleNode> curLevel = listIter.previous();
            for (TriangleNode node: curLevel) {
                node.computeWeights();
                if(node.isRoot()) {
                    rootNode = node;
                    break;
                }
            }
        }
        return extractMinPathFromRoot(rootNode);
    }

    /**
     * Takes as input the root node of the tree and returns a
     * TriangleMinPath object containing a list of the least weighted
     * nodes leading from the root to one of the least. This amounts to
     * the least weighted (minimal) path of this triangle tree.
     * @param root: The root node
     * @return  The Triangle tree's minimal path.
     */
    private TriangleMinPath extractMinPathFromRoot(TriangleNode root){
        List<TriangleNode> path = new ArrayList<>();
        int minWeight = root.getWeight();
        TriangleNode currentNode = root;
        while(currentNode!=null){
            path.add(currentNode);
            currentNode=currentNode.getLeastWeightedChild();
        }
        return new TriangleMinPath(path,minWeight);
    }


}
