import java.util.List;
import com.suprnation.assignment.datastructures.TriangleMinPath;
import com.suprnation.assignment.datastructures.TriangleNode;
import org.junit.Test;
import org.junit.Assert;
import com.suprnation.assignment.datastructures.TriangleTree;

public class TestTriangleTree
{
    @Test
    public void testAddInvalidLevel() {
        TriangleTree tree = new TriangleTree();
        final boolean wasLevelAdded = tree.addLevel(new int[] { 1, 2, 3 });
        Assert.assertFalse(wasLevelAdded);
    }

    @Test
    public void testAddValidLevel() {
        final TriangleTree tree = new TriangleTree();
        final boolean wasLevelAdded = tree.addLevel(new int[] { 1 });
        Assert.assertTrue(wasLevelAdded);
    }

    @Test
    public void testAddValidLevelFromText() {
        final TriangleTree tree = new TriangleTree();
        final boolean wasFirstLevelAdded = tree.addLevel("-7");
        final boolean wasSecondLevelAdded = tree.addLevel("6 3");
        final boolean wasThirdLevelAdded = tree.addLevel("3 8    -5");
        final boolean wasFourthLevelAdded = tree.addLevel("-11 2 10 9");
        final boolean allAdded = wasFirstLevelAdded && wasSecondLevelAdded && wasThirdLevelAdded && wasFourthLevelAdded;
        Assert.assertTrue(allAdded);
    }

    @Test
    public void testAddInvalidLevelFromText() {
        final TriangleTree tree = new TriangleTree();
        final boolean wasFirstLevelAdded = tree.addLevel("test");
        final boolean wasSecondLevelAdded = tree.addLevel("2 5");
        final boolean wasThirdLevelAdded = tree.addLevel("");
        final boolean noneAdded = !wasFirstLevelAdded && !wasSecondLevelAdded && !wasThirdLevelAdded;
        Assert.assertTrue(noneAdded);
    }

    @Test
    public void testCreateTree() {
        final TriangleTree tree = new TriangleTree();
        final boolean wasFirstLevelAdded = tree.addLevel(new int[] { 7 });
        final boolean wasSecondLevelAdded = tree.addLevel(new int[] { 6, 3 });
        final boolean wasThirdLevelAdded = tree.addLevel(new int[] { 3, 8, 5 });
        final boolean wasFourthLevelAdded = tree.addLevel(new int[] { 11, 2, 10, 9 });
        final boolean allAdded = wasFirstLevelAdded && wasSecondLevelAdded && wasThirdLevelAdded && wasFourthLevelAdded;
        Assert.assertTrue(allAdded);
        final TriangleNode root = tree.getLevel(0).get(0);
        final TriangleNode level1NodeA = root.getChildLeft();
        final TriangleNode level1NodeB = root.getChildRight();
        Assert.assertEquals((long)level1NodeA.getNumber(), 6L);
        Assert.assertEquals((long)level1NodeB.getNumber(), 3L);
        final TriangleNode level2NodeA = level1NodeA.getChildLeft();
        final TriangleNode level2NodeB = level1NodeA.getChildRight();
        final TriangleNode level2NodeC = level1NodeB.getChildRight();
        Assert.assertEquals((Object)level2NodeB, (Object)level1NodeB.getChildLeft());
        Assert.assertEquals((long)level2NodeA.getNumber(), 3L);
        Assert.assertEquals((long)level2NodeB.getNumber(), 8L);
        Assert.assertEquals((long)level2NodeC.getNumber(), 5L);
        final TriangleNode level3NodeA = level2NodeA.getChildLeft();
        final TriangleNode level3NodeB = level2NodeA.getChildRight();
        final TriangleNode level3NodeC = level2NodeC.getChildLeft();
        final TriangleNode level3NodeD = level2NodeC.getChildRight();
        Assert.assertEquals((Object)level3NodeB, (Object)level2NodeB.getChildLeft());
        Assert.assertEquals((Object)level3NodeC, (Object)level2NodeB.getChildRight());
        Assert.assertEquals((long)level3NodeA.getNumber(), 11L);
        Assert.assertEquals((long)level3NodeB.getNumber(), 2L);
        Assert.assertEquals((long)level3NodeC.getNumber(), 10L);
        Assert.assertEquals((long)level3NodeD.getNumber(), 9L);
    }

    @Test
    public void testComputeMinimalTreePosNumsOnly() {
        final TriangleTree tree = new TriangleTree();
        final boolean wasFirstLevelAdded = tree.addLevel(new int[] { 7 });
        final boolean wasSecondLevelAdded = tree.addLevel(new int[] { 6, 3 });
        final boolean wasThirdLevelAdded = tree.addLevel(new int[] { 3, 8, 5 });
        final boolean wasFourthLevelAdded = tree.addLevel(new int[] { 11, 2, 10, 9 });
        final boolean allAdded = wasFirstLevelAdded && wasSecondLevelAdded && wasThirdLevelAdded && wasFourthLevelAdded;
        Assert.assertTrue(allAdded);
        final TriangleMinPath minPath = tree.computeMinimalPath();
        Assert.assertEquals((long)minPath.getMinWeight(), 18L);
        final List<TriangleNode> path = (List<TriangleNode>)minPath.getPath();
        Assert.assertEquals((long)path.get(0).getNumber(), 7L);
        Assert.assertEquals((long)path.get(1).getNumber(), 6L);
        Assert.assertEquals((long)path.get(2).getNumber(), 3L);
        Assert.assertEquals((long)path.get(3).getNumber(), 2L);
    }

    @Test
    public void testComputeMinimalTreePosAndNegNums() {
        final TriangleTree tree = new TriangleTree();
        Assert.assertTrue(tree.addLevel("12"));
        Assert.assertTrue(tree.addLevel("-16 3"));
        Assert.assertTrue(tree.addLevel("3 15 5"));
        Assert.assertTrue(tree.addLevel("13 2 10 9"));
        Assert.assertTrue(tree.addLevel("14 4 -9 10 7"));
        Assert.assertTrue(tree.addLevel("6 3 -10 -15 -20 25"));
        final TriangleMinPath minPath = tree.computeMinimalPath();
        Assert.assertEquals((long)minPath.getMinWeight(), -23L);
    }

    @Test
    public void testComputeMinimalTreeNegNumsOnly() {
        final TriangleTree tree = new TriangleTree();
        Assert.assertTrue(tree.addLevel("-7"));
        Assert.assertTrue(tree.addLevel("-6 -3"));
        Assert.assertTrue(tree.addLevel("-3 -8 -5"));
        Assert.assertTrue(tree.addLevel("-11 -2 -10 -9"));
        final TriangleMinPath minPath = tree.computeMinimalPath();
        Assert.assertEquals((long)minPath.getMinWeight(), -31L);
    }
}